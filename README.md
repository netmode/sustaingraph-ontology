# SustainGraph-ontology

### Ontology Documentation
This repository contains information regarding the specification of the SustainGraph Ontology. The code regarding the documentation of the Ontology is available under the "public" folder, while an interactive documentation of the [SustainGraph Ontology](https://gitlab.com/netmode/sustaingraph-ontology/-/blob/main/public/ontology.ttl) can be found [here](https://netmode.gitlab.io/sustaingraph-ontology/).

## License
<img src="https://licensebuttons.net/l/by-nc-sa/3.0/88x31.png" alt="CC BY-NC-SA 4.0">

(c) 2022 by Eleni Fotopoulou, Ioanna Mandilara, Christina Maria Androna, Anastasios Zafeiropoulos (National Technical University of Athens)

## Cite
To cite this work, please use:

Fotopoulou E, Mandilara I, Zafeiropoulos A, Laspidou C, Adamos G, Koundouri P and Papavassiliou S (2022) SustainGraph: A knowledge graph for tracking the progress and the interlinking among the sustainable development goals’ targets. Front. Environ. Sci. 10:1003599. doi: [10.3389/fenvs.2022.1003599](https://www.frontiersin.org/articles/10.3389/fenvs.2022.1003599/full)
